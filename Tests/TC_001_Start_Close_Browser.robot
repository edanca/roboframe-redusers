*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/RSC_handle_browser.robot
Documentation  Testing using Robot Framework
...  for more see: https://robotframework.org
Metadata  Version 0.0.1

*** Variables ***
${ByNAME_Search}  name:s
${ById_VideoReviews}  id:menu_area_videoreviews
${ByCSS_SearchButton}  css:#searchsubmit
${ByID_SeachButton}  id:searchsubmit

*** Test Cases ***
Robot First Test case
    [Documentation]  FIRST TEST CASE OF THE LIST
    [Tags]  Search  Videos
    Open browser and maximize
    Search on textbox  firefox
    sleep  2
    Click on video reviews
    Close Browser Window

