*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${Browser}  Chrome
#${Browser}  Firefox
${URL}  http://redusers.com/


*** Keywords ***
Open browser and maximize
    open browser  ${url}  ${browser}
    maximize browser window

close Browser Window
    # ${title} = Get Title
    # Log ${title}
    close browser

Search on textbox
    [Arguments]  ${text_to_search}
    clear element text  ${byname_search}
    input text  ${ByName_Search}  ${text_to_search}
    click button  ${ByCSS_SearchButton}

Click on video reviews
    #click link  http://www.redusers.com/noticias/seccion/seccion_videoreviews/
    click element  ${ById_VideoReviews}

